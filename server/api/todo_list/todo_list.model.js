'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var ToDoSchema = new mongoose.Schema({
  title: String,
  tasks:[{
    name: String,
    active: Boolean
  }]
});

export default mongoose.model('ToDo', ToDoSchema);

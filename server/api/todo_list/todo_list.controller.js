/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/todos              ->  index
 * POST    /api/todos              ->  create
 * GET     /api/todos/:id          ->  show
 * PUT     /api/todos/:id          ->  update
 * DELETE  /api/todos/:id          ->  destroy
 */

'use strict';
import Todo from './todo_list.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    console.log("entity: >> " + entity);
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    res.status(statusCode).json(err);
  }
}

// Gets a list of Todos
export function index(req, res) {
  console.log('index');

  Todo.findAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Todo in the DB
export function create(req, res) {
  console.log('create');

  //var newTask = new Task(req.body);

  Todo.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Gets a single Task from the DB
export function show(req, res) {
  console.log('show');
}

// Updates an existing Todo in the DB
export function update(req, res) {
  console.log('update');

  //Todo.findByIdAsync(req.params.id)
  //  .then(handleEntityNotFound(res))
  //  .then(saveUpdates(req.body))
  //  .then(respondWithResult(res))
  //  .catch(handleError(res));

  Todo.findByIdAsync(req.params.id)
    .then(todo => {
      console.log("id:" + todo.id);
      console.log("old title: " + todo.title+" new: " + req.body.title);
      todo.title = req.body.title;
      todo.tasks = req.body.tasks;
      todo.save();
      //saveUpdates(req.body)
    })
    .catch(handleError(res));
}

// Deletes a Todo from the DB
export function destroy(req, res) {
  console.log('destroy');

  Todo.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}



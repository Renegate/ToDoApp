/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/tasks              ->  index
 * POST    /api/tasks              ->  create
 * GET     /api/tasks/:id          ->  show
 * PUT     /api/tasks/:id          ->  update
 * DELETE  /api/tasks/:id          ->  destroy
 */

'use strict';
import Task from './task.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Tasks
export function index(req, res) {
  console.log('index');

  Task.findAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Task in the DB
export function create(req, res) {
  console.log('create');

  //var newTask = new Task(req.body);

  Task.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Gets a single Task from the DB
export function show(req, res) {
  console.log('show');
}

// Updates an existing Task in the DB
export function update(req, res) {
  console.log('update');
}

// Deletes a Task from the DB
export function destroy(req, res) {
  console.log('destroy');

  Task.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}



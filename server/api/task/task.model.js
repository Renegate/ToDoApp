'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var TaskSchema = new mongoose.Schema({
  name: String,
  active: Boolean
});

export default mongoose.model('Task', TaskSchema);

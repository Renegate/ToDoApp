'use strict';

angular.module('toDoAppApp.auth', [
  'toDoAppApp.constants',
  'toDoAppApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });

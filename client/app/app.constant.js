(function(angular, undefined) {
'use strict';

angular.module('toDoAppApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);
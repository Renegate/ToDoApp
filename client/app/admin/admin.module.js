'use strict';

angular.module('toDoAppApp.admin', [
  'toDoAppApp.auth',
  'ui.router'
]);

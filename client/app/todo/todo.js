'use strict';

angular.module('toDoAppApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('todo', {
        url: '/todo',
        templateUrl: 'app/todo/todo.html',
        controller: 'ToDoController',
        controllerAs: 'todo'
      });
  });

'use strict';

(function() {

  class ToDoController {

    constructor($http, $scope, socket) {
      this.$http = $http;
      this.todos = [];
      this.current_todo = null;
      this.current_task = null;

      $http.get('/api/todos').then(response => {
        this.todos = response.data;
        socket.syncUpdates('todo', this.todos);
      });

      $scope.$on('$destroy', function() {
        socket.unsyncUpdates('todo');
      });
    }

    newTodo() {
      this.current_todo = {
        title: "",
        tasks: [{
          name: "",
          active: false
        }]
      }

      this.$http.post('/api/todos', this.current_todo);

      this.todos.push(this.current_todo);
    }

    newTask() {
      if (this.current_todo) {
        this.task = {
          name: "",
          active: false
        }
        this.current_todo.tasks.push(this.task);
        this.$http.put('/api/todos/'+this.current_todo._id, this.current_todo);
      }
    }

    saveTask() {
      if (this.current_todo && this.newTask) {
        this.$http.post('/api/todos', this.newTask );
        this.newTodo = '';
      }
    }

    todoChange(todo) {
      if(todo) {
        this.current_todo = todo;
        this.$http.put('/api/todos/'+this.current_todo._id, this.current_todo);
        //this.current_todo = null;
      }
    }

    taskChange(task) {
      if(this.current_todo && task) {
        this.current_task = task;

        this.$http.put('/api/todos/'+this.current_todo._id, this.current_todo);
        //this.current_todo = null;
      }
    }

    deleteThing(thing) {
      this.$http.delete('/api/todos/' + todo._id);
    }

  }

  angular.module('toDoAppApp')
    .controller('ToDoController', ToDoController);

})();
